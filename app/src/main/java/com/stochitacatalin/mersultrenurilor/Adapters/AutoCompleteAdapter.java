package com.stochitacatalin.mersultrenurilor.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.stochitacatalin.mersultrenurilor.ArrayListCustom;
import com.stochitacatalin.mersultrenurilor.Date.StatieItem;
import com.stochitacatalin.mersultrenurilor.Date.StatieItemp;
import com.stochitacatalin.mersultrenurilor.Date.TrenItem;
import com.stochitacatalin.mersultrenurilor.Date.TrenItemp;

import static com.stochitacatalin.mersultrenurilor.Utils.bina;

public class AutoCompleteAdapter<T> extends BaseAdapter implements Filterable {

    private ArrayListCustom autoComplete;
    private ArrayListCustom<T> list;
    private int selected = -1;
    private Context context;
    private int resource;
    private int type;

    public AutoCompleteAdapter(Context context, int resource,ArrayListCustom<T> list,int type) {
        super();
        this.resource = resource;
        this.context = context;
        this.list = list;
        this.type = type;
    }

    public int getSelected(){ return selected;}
    public void setSelected(int position){selected = position;}
    @Override
    public int getCount() {
        return autoComplete.size();
    }

    @Override
    public String getItem(int position) {
        if (type == 1)
            return ((TrenItem) autoComplete.get(position)).getNumar();
        else
            return (String) ((Object[]) autoComplete.get(position))[1];
    }

    public int getItemI(int position){
        return (int)((Object[])autoComplete.get(position))[0];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);

        if (convertView == null) {
            convertView = inflater.inflate(resource, parent, false);
        }

        TextView text1 = convertView.findViewById(android.R.id.text1);
        Object temp = autoComplete.get(position);
        if(temp instanceof TrenItem)
            text1.setText(((TrenItem)temp).getNumar());
        else
            text1.setText((String)((Object[])temp)[1]);

        return convertView;
    }

    public Object getItemO(int position){return autoComplete.get(position);}

    @NonNull
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(final CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    selected = -1;
                    int count = 0;
                    autoComplete = new ArrayListCustom();
                    Log.d("size", String.valueOf(list.size()));
                    for (int i = 0; i < list.size() && count < 3; i++) {
                        if(list.get(i) instanceof StatieItemp) {
                            StatieItemp temp = ((StatieItemp) list.get(i));
                            String nume = temp.getNume();
                            if (bina(nume,constraint.toString())){
                                autoComplete.add(new Object[]{i,temp.getNume()});
                                count ++;
                            }
                        }
                        else if(list.get(i) instanceof TrenItemp){
                            TrenItem temp = ((TrenItemp) list.get(i)).getItem();
                            String nume = temp.getNumar();
                            if (bina(nume,constraint.toString())){
                                autoComplete.add(temp);
                                count ++;
                            }
                        }

                    }
                    filterResults.values = autoComplete;
                    filterResults.count = autoComplete.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
    }
}