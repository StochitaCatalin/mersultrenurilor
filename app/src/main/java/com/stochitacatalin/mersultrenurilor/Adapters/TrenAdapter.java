package com.stochitacatalin.mersultrenurilor.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.stochitacatalin.mersultrenurilor.ArrayListCustom;
import com.stochitacatalin.mersultrenurilor.Date.TrenData;
import com.stochitacatalin.mersultrenurilor.MainActivity;
import com.stochitacatalin.mersultrenurilor.R;

import java.text.SimpleDateFormat;

import static com.stochitacatalin.mersultrenurilor.Utils.inttotime;
/*
public class TrenAdapter extends ArrayAdapter<TrenData> {
    public TrenAdapter(Context context, ArrayListCustom trains) {
        super(context, 0, trains.toALTD());
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

    }
}*/
public class TrenAdapter extends BaseAdapter {

    private int enableDate;
    private ArrayListCustom trenuri;
    private LayoutInflater layoutInflater;

    public TrenAdapter(Context context, ArrayListCustom trenuri,int enableDate) {
        super();
        this.enableDate = enableDate;
        this.trenuri = trenuri;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return trenuri.size();
    }

    @Override
    public TrenData getItem(int position) {
        return (TrenData) trenuri.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("ViewHolder")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        TrenData tren = getItem(position);
        convertView= layoutInflater.inflate(R.layout.tren_custom_list, parent,false);
        TextView trennr = convertView.findViewById(R.id.trennr);
        TextView plecaretimp = convertView.findViewById(R.id.plecaretimp);
        TextView plecarenume = convertView.findViewById(R.id.plecarenume);
        TextView sosiretimp = convertView.findViewById(R.id.sosiretimp);
        TextView sosirenume = convertView.findViewById(R.id.sosirenume);
        TextView date = convertView.findViewById(R.id.dateText);

        if (tren != null) {
            trennr.setText(MainActivity.trenurip.getItem(tren.getTrenItem()).getNumar());
            plecaretimp.setText(inttotime(tren.getPlecaretimp()));
            plecarenume.setText(MainActivity.statiip.getItemp(tren.getPlecare()).getNume());
            sosiretimp.setText(inttotime(tren.getSosiretimp()));
            sosirenume.setText(MainActivity.statiip.getItemp(tren.getSosire()).getNume());
            if(enableDate == 1) {
                date.setVisibility(View.VISIBLE);
                @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                date.setText(formatter.format(tren.getDate()));
            }
        }

        return convertView;
    }

}