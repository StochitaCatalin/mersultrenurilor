package com.stochitacatalin.mersultrenurilor.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.stochitacatalin.mersultrenurilor.ArrayListCustom;
import com.stochitacatalin.mersultrenurilor.Date.StatieData;
import com.stochitacatalin.mersultrenurilor.Date.StatieItem;
import com.stochitacatalin.mersultrenurilor.MainActivity;
import com.stochitacatalin.mersultrenurilor.R;

import java.util.ArrayList;
import java.util.List;

public class StatieAdapter extends BaseAdapter {
    private ArrayListCustom<StatieData> statii;
    private int sosire,plecare;
    private LayoutInflater layoutInflater;

    public StatieAdapter(Context context, ArrayListCustom<StatieData> statii,int sosire,int plecare) {
        super();
        this.statii = statii;
        this.sosire = sosire;
        this.plecare = plecare;
        layoutInflater = LayoutInflater.from(context);
    }
    public StatieAdapter(Context context, ArrayListCustom<StatieData> statii) {
        super();
        this.statii = statii;
        this.sosire = -1;
        this.plecare = -1;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return statii.size();
    }

    @Override
    public StatieData getItem(int position) {
        return statii.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        StatieData statie = getItem(position);
        convertView= layoutInflater.inflate(R.layout.statie_custom_list, parent,false);
        TextView snume = convertView.findViewById(R.id.nume);
        TextView ssosire = convertView.findViewById(R.id.sosire);
        TextView splecare = convertView.findViewById(R.id.plecare);
        ImageView sinatren = convertView.findViewById(R.id.sinatren);
        if (statie != null) {
            snume.setText(statie.getNume());
            ssosire.setText(statie.getSosire());
            splecare.setText(statie.getPlecare());
            Log.d("img", String.valueOf(statie.getImg()));
            if(statie.getImg()!=0){
                switch (statie.getImg()){
                    case 1:
                        sinatren.setImageDrawable(layoutInflater.getContext().getDrawable(R.drawable.tren));
                        break;
                    case 2:
                        sinatren.setImageDrawable(layoutInflater.getContext().getDrawable(R.drawable.trensus));
                        break;
                    case 3:
                        sinatren.setImageDrawable(layoutInflater.getContext().getDrawable(R.drawable.trenjos));
                        break;
                }
            }
            if(sosire != -1 && plecare != -1) {
                if (statie.getId() == sosire || statie.getId() == plecare) {
                    /*
                    snume.setTextColor(layoutInflater.getContext().getColor(R.color.colorRed));
                    ssosire.setTextColor(layoutInflater.getContext().getColor(R.color.colorRed));
                    splecare.setTextColor(layoutInflater.getContext().getColor(R.color.colorRed));
                    */
                    snume.setTextColor(ContextCompat.getColor(layoutInflater.getContext(), R.color.colorRed));
                    ssosire.setTextColor(ContextCompat.getColor(layoutInflater.getContext(), R.color.colorRed));
                    splecare.setTextColor(ContextCompat.getColor(layoutInflater.getContext(), R.color.colorRed));
                }
            }
        }
        return convertView;
    }
}
