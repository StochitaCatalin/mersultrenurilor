package com.stochitacatalin.mersultrenurilor;

import android.util.Log;

class ArrayListItem<T>{
    private T data;
    ArrayListItem<T> precedent;
    ArrayListItem<T> urmatorul;
    ArrayListItem(T data){
        this.data = data;
        this.urmatorul = null;
        this.precedent = null;
    }
    public T getData(){return data;}
}

public class ArrayListCustom<T> {
    private ArrayListItem<T> inceput;
    private ArrayListItem<T> sfarsit;
    private ArrayListItem<T> current;
    private int currenti;
    private int size;
    public ArrayListCustom(){
        inceput = null;
        sfarsit = null;
        current = null;
        currenti = -1;
        size = 0;
    }
    public void clear(){
        this.size = 0;
        this.inceput = null;
    }
    public void add(T object){
        if(inceput == null){
            inceput = new ArrayListItem<>(object);
            sfarsit = inceput;
        }
        else{
            ArrayListItem<T> temp = sfarsit;
            temp.urmatorul = new ArrayListItem<>(object);
            temp.urmatorul.precedent = sfarsit;
            sfarsit = temp.urmatorul;
        }
        size = size + 1;
    }
    public void remove(int i){
        if(i == 0){
            inceput = inceput.urmatorul;
            inceput.precedent = null;
            if(size == 1)
                sfarsit = null;
        }
        else if(i == size -1){
            ArrayListItem<T> temp = sfarsit;
            temp.precedent.urmatorul = null;
            sfarsit = temp.precedent;
        }
        else{
            ArrayListItem<T> temp = inceput;
            while(i>1){
                temp = temp.urmatorul;
                i--;
            }
            temp.urmatorul = temp.urmatorul.urmatorul;
            temp.urmatorul.precedent = temp;
        }
        currenti = -1;
        current = null;
        size = size - 1;
    }
    public boolean contains(T object,int type){
        ArrayListItem temp = inceput;
        while(temp!=null) {
                if (temp.getData() == object)
                    return true;
            temp = temp.urmatorul;
        }
        return false;
    }
    public int size(){return size;}
    private ArrayListItem<T> ALIP(ArrayListItem<T> item, int i){
        while(i > 0){
            item = item.precedent;
            i--;
        }
        return item;
    }
    private ArrayListItem<T> ALIU(ArrayListItem<T> item, int i){
        while(i > 0){
            item = item.urmatorul;
            i--;
        }
        return item;
    }
    public T get(int i) {
        ArrayListItem<T> temp = inceput;
        int ii = i;
        if(size - i -1 < i){
            int pas = size - i -1;
            if(current != null){
                boolean urm = true;
                int p = currenti - i;
                if(p<0){
                    p = -p;
                    urm = false;
                }
                if(p < pas){
                    if(!urm)
                        temp = ALIU(current,p);
                    else
                        temp = ALIP(current,p);
                }
                else
                    temp = ALIP(sfarsit,pas);
            }
            else
                temp = ALIP(sfarsit,pas);
        }
        else {
            if(current != null){
                boolean urm = true;
                int p = currenti - i;
                if(p<0){
                    p = -p;
                    urm = false;
                }
                if(p < i){
                    if(!urm)
                        temp = ALIU(current,p);
                    else
                        temp = ALIP(current,p);
                }
                else
                    temp = ALIU(temp,i);
            }
            else
                temp = ALIU(temp,i);
        }

        currenti = ii;
        current = temp;

        return temp.getData();
    }
    /*
    public Object get(int i){
        ArrayListItem temp = inceput;
        while(i>0){
            temp = temp.urmatorul;
            i--;
        }
        return temp.getData();
    }*/
   /* public ArrayListCustom<T> getA(int i){
        ArrayListItem<T> temp = this.inceput;
        while(i>0){
            temp = temp.urmatorul;
            i--;
        }
        return temp.getData();
    }*/
}
