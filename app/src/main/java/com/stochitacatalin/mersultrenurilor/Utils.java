package com.stochitacatalin.mersultrenurilor;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import com.stochitacatalin.mersultrenurilor.Date.StatieItemp;
import com.stochitacatalin.mersultrenurilor.Date.TrenItemp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Utils {
    public static String inttotime(int time){
        if(time<0)
            return "---";
        int orap = time%86400;
        float ora = (float)orap/3600;
        int ora2 = (int) ora;
        int ora3 = (int) ((ora - ora2)*60);
        String toret = "";
        if(ora2<10)
            toret += "0";
        toret+=ora2+":";
        if(ora3<10)
            toret += "0";
        toret+=ora3;
        return toret;
    }
    public static String JSONFileFromAsset (String filename, Context context) throws IOException {
        AssetManager manager = context.getAssets();
        InputStream file = manager.open(filename);

        byte[] formArray = new byte[file.available()];
        file.read(formArray);
        file.close();

        return new String(formArray);
    }
    public static String FromFile(String filename,int offset,int length,Context context) throws IOException {
        AssetManager manager = context.getAssets();
        InputStream file = manager.open(filename);
        BufferedReader br = new BufferedReader(new InputStreamReader(file));
        char[] bt = new char[length];
        br.skip(offset);
        br.read(bt);
        file.close();
        return new String(bt);
    }
    public static ArrayListCustom<StatieItemp> StatiiFileFromAsset (String filename, Context context) throws IOException {
        AssetManager manager = context.getAssets();
        InputStream file = manager.open(filename);
        ArrayListCustom<StatieItemp> als = new ArrayListCustom<>();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                String split[] = line.split(" ");
                als.add(new StatieItemp(Integer.parseInt(split[0]),Integer.parseInt(split[1]),line.substring(split[0].length() + split[1].length() + 1)));
            }
        }
        file.close();

        return als;
    }
    public static ArrayListCustom<TrenItemp> TrenuriFileFromAsset (String filename, Context context) throws IOException {
        AssetManager manager = context.getAssets();
        InputStream file = manager.open(filename);
        ArrayListCustom<TrenItemp> als = new ArrayListCustom();
        try (BufferedReader br = new BufferedReader(new InputStreamReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                String split[] = line.split(" ");
                als.add(new TrenItemp(Integer.parseInt(split[0]),Integer.parseInt(split[1])));
            }
        }
        file.close();

        return als;
    }
    public static JSONArray loadfromfile(Context context,String name){
        JSONArray jsonArray = new JSONArray();
        try {
            FileInputStream fis = new FileInputStream (new File(context.getFilesDir() + File.separator +name));
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader bufferedReader = new BufferedReader(isr);
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                sb.append(line);
            }
            jsonArray = new JSONArray(sb.toString());
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
        return jsonArray;
    }
    public static void savetofile(Context context,String name,JSONArray jsonArray){
        try {
            Writer output;
            File file = new File(context.getFilesDir() + File.separator + name);
            output = new BufferedWriter(new FileWriter(file));
            output.write(jsonArray.toString());
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Date curentdate() {
        @SuppressLint("SimpleDateFormat") DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date azicutimp = new Date();
        Date azi = null;
        try {
            azi = formatter.parse(formatter.format(azicutimp));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return azi;
    }
    public static Date curenttime() {
        @SuppressLint("SimpleDateFormat") DateFormat formatter = new SimpleDateFormat("HH:mm");
        Date azicutimp = new Date();
        Date azi = null;
        try {
            azi = formatter.parse(formatter.format(azicutimp));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return azi;
    }

    public static boolean circulatrenul(Date dela, Date panala, String zile,Date azi) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(azi);
        int zi = cal.get(Calendar.DAY_OF_WEEK);
        return zile.charAt(zi - 1 < 0?6:zi - 1) == '1' && dela.compareTo(azi) <= 0 && panala.compareTo(azi) >= 0;
    }
    private static char chardiacritice(char a){
        if(a == 'Ş' || a == 'ş')
            a = 's';
        else if(a == 'Ţ' || a == 'ţ')
            a = 't';
        else if(a == 'Î' || a == 'î')
            a = 'i';
        else if(a == 'Ă' || a == 'ă' || a == 'Â' || a == 'â')
            a = 'a';
        if(a > 64 && a < 91)
            a = (char) (a + 32);
        return a;
    }

    private static boolean aisb(char a, char b){
        return chardiacritice(a) == chardiacritice(b);
    }

    public static boolean bina(String a, String b){
        if(b.length() == 0)
            return true;
        for(int i = 0;i<a.length();i++){
            if(b.length() + i > a.length())
                break;
            int k;
            for(k = 0;k<b.length();k++)
                if(!aisb(a.charAt(i+k),b.charAt(k)))
                    break;
            if(k==b.length())
                return true;
        }
        return false;
    }
}
