package com.stochitacatalin.mersultrenurilor.Date;

import com.stochitacatalin.mersultrenurilor.ArrayListCustom;

import org.json.JSONArray;
import org.json.JSONException;

public class StatieItem {
    private int id;
    private int numar;
   // private String nume;
    private ArrayListCustom<Integer> trenuri;
    StatieItem(int numar, JSONArray trenuri) {
        //this.id = id;
        this.numar = numar;
       // this.nume = nume;
        this.trenuri = new ArrayListCustom<>();
        try {
            for (int i = 0; i < trenuri.length(); i++) {
                this.trenuri.add(trenuri.getInt(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();

        }
    }
  //  public String getNume(){
  //      return this.nume;
  //  }
    public int getNumar() {
        return numar;
    }

    public int getId() {
        return id;
    }

    public ArrayListCustom<Integer> getTrenuri() {
        return trenuri;
    }
}
