package com.stochitacatalin.mersultrenurilor.Date;

import java.util.Date;

public class TrenData {
    private int plecare;
    private int plecaretimp;
    private int sosire;
    private int sosiretimp;
    private Date date;
    private int trenItem;

    public TrenData(int trenItem, int plecare, int plecaretimp, int sosire, int sosiretimp,Date date) {
        this.trenItem = trenItem;
        this.plecare = plecare;
        this.plecaretimp = plecaretimp;
        this.sosire = sosire;
        this.sosiretimp = sosiretimp;
        this.date = date;
    }
    //public int getTrenid(){return this.trenid;}
    //public String getTrennr(){return this.trennr;}
    public int getPlecare(){return this.plecare;}
    public int getPlecaretimp(){return this.plecaretimp;}
    public int getSosire(){return this.sosire;}
    public int getSosiretimp(){return this.sosiretimp;}
    public Date getDate() {
        return date;
    }
    public int getTrenItem() {
        return trenItem;
    }
}
