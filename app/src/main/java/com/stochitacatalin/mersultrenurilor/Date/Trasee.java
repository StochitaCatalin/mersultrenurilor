package com.stochitacatalin.mersultrenurilor.Date;

import com.stochitacatalin.mersultrenurilor.ArrayListCustom;
import com.stochitacatalin.mersultrenurilor.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import static com.stochitacatalin.mersultrenurilor.Utils.FromFile;

public class Trasee {
    ArrayListCustom<TraseeItem> trasee;
    int offset;
    int length;
    Trasee(int offset,int length){
        this.offset = offset;
        this.length = length;
        trasee = null;
    }
    void loadtrasee(){
        try {
            JSONArray jsonArray = new JSONArray(FromFile("trasee.json",offset,length, MainActivity.context));
            trasee = new ArrayListCustom<>();
            for(int i = 0;i<jsonArray.length();i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                String tiporipre = obj.getString("TipOprire");
               // int secventa = obj.getInt("Secventa");
                trasee.add(new TraseeItem(obj.getInt("Statie"), obj.getInt("OraS"), obj.getInt("OraP"), tiporipre));
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

    }
    public int size(){
        if(trasee == null)
            loadtrasee();
        return trasee.size();
    }
    TraseeItem getItem(int i){
        if(trasee == null)
            loadtrasee();
        return trasee.get(i);
    }

    public ArrayListCustom<TraseeItem> getTrasee() {
        if(trasee == null)
            loadtrasee();
        return trasee;
    }
}
