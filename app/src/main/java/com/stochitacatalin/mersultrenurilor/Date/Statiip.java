package com.stochitacatalin.mersultrenurilor.Date;

import android.content.Context;
import android.util.Log;

import com.stochitacatalin.mersultrenurilor.ArrayListCustom;

import java.io.IOException;
import java.util.Objects;

import static com.stochitacatalin.mersultrenurilor.Utils.StatiiFileFromAsset;

public class Statiip{
    public ArrayListCustom<StatieItemp> statiip;
    public Statiip(Context context){
        statiip = new ArrayListCustom();
        try {
            statiip = StatiiFileFromAsset("statiip", context);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public StatieItemp getItemp(int i){return statiip.get(i);}
    public StatieItem getItem(int i){
        return statiip.get(i).getItem();
    }
}

