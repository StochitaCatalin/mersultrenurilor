package com.stochitacatalin.mersultrenurilor.Date;

import android.content.Context;

import com.stochitacatalin.mersultrenurilor.ArrayListCustom;

import java.io.IOException;

import static com.stochitacatalin.mersultrenurilor.Utils.StatiiFileFromAsset;
import static com.stochitacatalin.mersultrenurilor.Utils.TrenuriFileFromAsset;

public class Trenurip{
    public ArrayListCustom<TrenItemp> trenurip;
    public Trenurip(Context context){
        trenurip = new ArrayListCustom();
        try {
            trenurip = TrenuriFileFromAsset("trenurip", context);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public TrenItem getItem(int i){
        return (trenurip.get(i)).getItem();
    }
}
