package com.stochitacatalin.mersultrenurilor.Date;

import android.annotation.SuppressLint;

import com.stochitacatalin.mersultrenurilor.ArrayListCustom;
import com.stochitacatalin.mersultrenurilor.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.stochitacatalin.mersultrenurilor.Utils.FromFile;

public class Restrictii {
    private int offset;
    private int length;
    private ArrayListCustom<RestrictieItem> restrictii;
    public Restrictii(int offset, int length){
        this.offset = offset;
        this.length = length;
        restrictii = null;
    }
    void loadrestrictii(){
        try {
            JSONArray jsonArray = new JSONArray(FromFile("restrictii.json", offset,length, MainActivity.context));
            restrictii = new ArrayListCustom();
            for(int i = 0;i<jsonArray.length();i++) {
                JSONObject obj = (JSONObject) jsonArray.get(i);
                @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date dela = formatter.parse(obj.getString("DeLa"));
                    Date panala = formatter.parse(obj.getString("PinaLa"));
                    restrictii.add(new RestrictieItem(dela, panala, (String) obj.get("zile")));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }
    public int size(){
        if(restrictii == null)
            loadrestrictii();
        return restrictii.size();
    }
    public RestrictieItem getItem(int i){
        if(restrictii == null)
            loadrestrictii();

        return restrictii.get(i);
    }

    public ArrayListCustom<RestrictieItem> getRestrictii() {
        if(restrictii == null)
            loadrestrictii();
        return restrictii;
    }
}
