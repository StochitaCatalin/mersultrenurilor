package com.stochitacatalin.mersultrenurilor.Date;

public class StatieData {
    private int id;
    private String nume;
    private String plecare;
    private String sosire;
    private int plecareint;
    private int sosireint;
    private int img;
    StatieData(){
        this.nume = null;
        this.plecare = null;
        this.sosire = null;
        this.img = 0;
    }

    public StatieData(int id,String nume, String plecare, String sosire) {
        this.id = id;
        this.nume = nume;
        this.plecare = plecare;
        this.sosire = sosire;
    }

    public void setImg(int img){
        this.img = img;
    }

    public int getImg() {
        return img;
    }

    public String getNume(){return nume;}
    public String getPlecare(){return plecare;}
    public String getSosire(){return sosire;}
    public int getPlecareint(){
        return plecareint;
    }

    public int getSosireint() {
        return sosireint;
    }

    public int getId() {
        return id;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    void setPlecare(String plecare,int plecareint) {
        this.plecare = plecare;
        this.plecareint = plecareint;
    }

    void setSosire(String sosire,int sosireint) {
        this.sosire = sosire;
        this.sosireint = sosireint;
    }

    public void setId(int id) {
        this.id = id;
    }
}
