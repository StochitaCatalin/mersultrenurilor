package com.stochitacatalin.mersultrenurilor.Date;

import android.content.Context;

import com.stochitacatalin.mersultrenurilor.ArrayListCustom;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;

import static com.stochitacatalin.mersultrenurilor.Utils.JSONFileFromAsset;

public class Statii{
    public ArrayListCustom statii;
    public Statii(Context context){
        statii = new ArrayListCustom();
        int numar;
        String nume;
        try {
            String jsonFile = JSONFileFromAsset("statii.json", context);
            JSONArray jsonArray = new JSONArray(jsonFile);
            for(int i=0;i<jsonArray.length();i++)
            {
                JSONObject jsonObject =(JSONObject) jsonArray.get(i);
                numar = jsonObject.getInt("numar");
                nume = jsonObject.getString("nume");
                //statii.add(new StatieItem(i,numar,nume));
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
    }
    StatieItem statiebynumar(int numar){
        for(int i = 0;i<statii.size();i++){
            StatieItem temp = (StatieItem) statii.get(i);
            if(temp.getNumar() == numar)
                return temp;
        }
        return null;
    }

}

