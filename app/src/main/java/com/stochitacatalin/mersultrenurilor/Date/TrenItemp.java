package com.stochitacatalin.mersultrenurilor.Date;

import com.stochitacatalin.mersultrenurilor.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import static com.stochitacatalin.mersultrenurilor.Utils.FromFile;

public class TrenItemp {
    private int offset;
    private int length;
    private TrenItem trenItem;
    public TrenItemp(int offset, int length){
        this.offset = offset;
        this.length = length;
        trenItem = null;
    }
    public TrenItem getItem(){
        if(trenItem != null)
            return trenItem;
        try {
            String jsonFile = FromFile("trenuri.json", offset,length, MainActivity.context);
            JSONObject jsonObject = new JSONObject(jsonFile);
            String numar = jsonObject.getString("numar");
            String numar2 = jsonObject.getString("numar2");
            int operator = jsonObject.getInt("operator");
            int rang = jsonObject.getInt("rang");
            int servicii = jsonObject.getInt("servicii");
            String categorie = jsonObject.getString("categorie");
            int codstatieinitiala = jsonObject.getInt("CodStatieInitiala");
            int codstatiefinala = jsonObject.getInt("CodStatieFinala");
            JSONArray restrictii = jsonObject.getJSONArray("restrictii");
            JSONArray trasee = jsonObject.getJSONArray("trasee");
            trenItem = new TrenItem(numar,numar2,operator,rang,servicii,categorie,codstatieinitiala,codstatiefinala,restrictii,trasee);
            return trenItem;
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
