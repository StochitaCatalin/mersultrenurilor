package com.stochitacatalin.mersultrenurilor.Date;

public class TraseeItem {
    private int statie;
    private int km;
    private int orap;
    private int oras;
    private String tipoprire;
    TraseeItem(int statie, int oras, int orap, String tipoprire) {
        this.statie = statie;
        this.orap = orap;
        this.oras = oras;
        this.tipoprire = tipoprire;
    }
    /*TraseeItem(int statieorigine, int statiedestinatie, int km, int orap, int oras, int secventa, int stationaresecunde, String tipoprire){
        this.statieorigine = statieorigine;
        this.statiedestinatie = statiedestinatie;
        this.km = km;
        this.orap = orap;
        this.oras = oras;
        this.secventa = secventa;
        this.stationaresecunde = stationaresecunde;
        this.tipoprire = tipoprire;
    }*/

    public int getStatie(){
        return statie;
    }

    public String getTipoprire() {
        return tipoprire;
    }

    public int getOrap() {
        return orap;
    }

    public int getOras() {
        return oras;
    }
}
