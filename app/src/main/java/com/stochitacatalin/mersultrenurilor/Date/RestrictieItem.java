package com.stochitacatalin.mersultrenurilor.Date;

import java.util.Date;

public class RestrictieItem{
    private Date dela;
    private Date panala;
    private String zile;
    RestrictieItem(Date dela, Date panala, String zile){
        this.dela = dela;
        this.panala = panala;
        this.zile = zile;
    }
    public Date getDela(){
        return dela;
    }
    public Date getPanala(){
        return panala;
    }
    public String getZile(){
        return zile;
    }
}
