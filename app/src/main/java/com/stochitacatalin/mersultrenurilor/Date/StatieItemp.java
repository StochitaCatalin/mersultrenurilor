package com.stochitacatalin.mersultrenurilor.Date;

import android.util.Log;

import com.stochitacatalin.mersultrenurilor.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import static com.stochitacatalin.mersultrenurilor.Utils.FromFile;

public class StatieItemp {
    private int offset;
    private int length;
    private StatieItem statieItem;
    private String nume;
    public StatieItemp(int offset, int length,String nume){
        this.offset = offset;
        this.length = length;
        this.nume = nume;
        statieItem = null;
    }
    public StatieItem getItem(){
        if(statieItem != null)
            return statieItem;
        try {
            String jsonFile = FromFile("statii.json", offset,length,MainActivity.context);
            JSONObject jsonObject = new JSONObject(jsonFile);
            int numar = jsonObject.getInt("numar");
           // String nume = jsonObject.getString("nume");
            statieItem = new StatieItem(numar, (JSONArray) jsonObject.get("trenuri"));
            return statieItem;
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    public String getNume(){
        return this.nume;
    }
}
