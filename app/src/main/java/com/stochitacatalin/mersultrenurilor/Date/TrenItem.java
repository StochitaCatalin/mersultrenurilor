package com.stochitacatalin.mersultrenurilor.Date;

import com.stochitacatalin.mersultrenurilor.ArrayListCustom;
import com.stochitacatalin.mersultrenurilor.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;

import static com.stochitacatalin.mersultrenurilor.Utils.inttotime;

public class TrenItem {
    private int id;
    private String numar;
    private String numar2;
    private int operator;
    private int rang;
    private int servicii;
    private String categorie;
    private int codstatieinitiala;
    private int codstatiefinala;
    private Restrictii restrictii;
    private Trasee trasee;
    TrenItem(String numar, String numar2, int operator, int rang, int servicii, String categorie, int codstatieinitiala, int codstatiefinala, JSONArray restrictii, JSONArray trasee){
       // this.id = id;
        this.numar = numar;
        this.numar2 = numar2;
        this.operator = operator;
        this.rang = rang;
        this.servicii = servicii;
        this.categorie = categorie;
        this.codstatieinitiala = codstatieinitiala;
        this.codstatiefinala = codstatiefinala;
        try {
            this.restrictii = new Restrictii(restrictii.getInt(0),restrictii.getInt(1));
            this.trasee = new Trasee(trasee.getInt(0),trasee.getInt(1));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public String getNumar(){
        return this.numar;
    }
    public int statietimp(int statieItem,int mod){
        //not yet

        for(int i = 0;i<trasee.size();i++) {
            TraseeItem traseeItem = trasee.getItem(i);
            if (traseeItem.getStatie() == statieItem)
                if (mod == 1) {
                    return traseeItem.getOrap();
                } else {
                    return traseeItem.getOras();
                }
        }
        return 0;
    }
    public int getId() {
        return id;
    }

    public ArrayListCustom<StatieData> cautastatii() {
        ArrayListCustom<StatieData> ret = new ArrayListCustom<>();
        StatieData sd = null;
        for (int i = 0; i < trasee.size(); i++) {
            TraseeItem temp = trasee.getItem(i);
            sd = new StatieData();
            sd.setId(temp.getStatie());
            sd.setNume(MainActivity.statiip.getItemp(temp.getStatie()).getNume());
            sd.setSosire(inttotime(temp.getOras()), temp.getOras());
            sd.setPlecare(inttotime(temp.getOrap()), temp.getOrap());
            ret.add(sd);
        }
        return ret;
    }

    public ArrayListCustom<RestrictieItem> getRestrictii() {
        return restrictii.getRestrictii();
    }

    public ArrayListCustom<TraseeItem> getTrasee() {
        return trasee.getTrasee();
       // return trasee.trasee;
    }
}
