package com.stochitacatalin.mersultrenurilor;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.stochitacatalin.mersultrenurilor.Date.Statii;
import com.stochitacatalin.mersultrenurilor.Date.Statiip;
import com.stochitacatalin.mersultrenurilor.Date.TraseeItem;
import com.stochitacatalin.mersultrenurilor.Date.TrenData;
import com.stochitacatalin.mersultrenurilor.Date.TrenItem;
import com.stochitacatalin.mersultrenurilor.Date.TrenItemp;
import com.stochitacatalin.mersultrenurilor.Date.Trenuri;
import com.stochitacatalin.mersultrenurilor.Date.Trenurip;
import com.stochitacatalin.mersultrenurilor.Fragmente.AcasaFragment;
import com.stochitacatalin.mersultrenurilor.Fragmente.RuteFragment;
import com.stochitacatalin.mersultrenurilor.Fragmente.StatieInfoFragment;
import com.stochitacatalin.mersultrenurilor.Fragmente.StatiiFragment;
import com.stochitacatalin.mersultrenurilor.Fragmente.TrenInfoFragment;
import com.stochitacatalin.mersultrenurilor.Fragmente.TrenuriFragment;

public class MainActivity extends AppCompatActivity implements
        AcasaFragment.OnFragmentInteractionListener,
        RuteFragment.OnFragmentInteractionListener,
        StatiiFragment.OnFragmentInteractionListener,
        TrenuriFragment.OnFragmentInteractionListener,
        TrenInfoFragment.OnFragmentInteractionListener,
        StatieInfoFragment.OnFragmentInteractionListener{

    Fragment acasaFragment = null;
    Fragment ruteFragment = null;
    Fragment statiiFragment = null;
    Fragment trenuriFragment = null;
    FragmentTransaction ft;
    public static Statiip statiip;
    public static Trenurip trenurip;
    public static Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = findViewById(R.id.navigatie);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        context = getApplicationContext();
        statiip = new Statiip(getApplicationContext());
        trenurip = new Trenurip(getApplicationContext());

        ft = getSupportFragmentManager().beginTransaction();
        navigation.setSelectedItemId(R.id.navigation_acasa);
/*
        for(int i = 0;i<trenurip.trenurip.size();i++){
            TrenItem trenItem = trenurip.getItem(i);
            if(trenItem.getNumar().equals("1695")){
                Log.d("Tren gasit", String.valueOf(i));
                ArrayListCustom<TraseeItem> tras = trenItem.getTrasee();
                for(int j = 0;j<tras.size();j++){
                    TraseeItem trase = tras.get(j);
                    Log.d(statiip.getItemp(trase.getStatie()).getNume(), String.valueOf(trase.getOras()));
                }
            }

        }*/
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            switch (item.getItemId()) {

                case R.id.navigation_acasa:

                    if (acasaFragment == null) {
                        acasaFragment = new AcasaFragment();
                    }
                    transaction.replace(R.id.content, acasaFragment, "acasa");
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    transaction.commit();
                    return true;
                case R.id.navigation_rute:

                    if (ruteFragment == null) {
                        ruteFragment = new RuteFragment();
                    }

                    transaction.replace(R.id.content, ruteFragment, "rute");
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    transaction.commit();
                    return true;
                case R.id.navigation_trenuri:
                    if (trenuriFragment == null) {
                        trenuriFragment = new TrenuriFragment();
                    }
                    transaction.replace(R.id.content, trenuriFragment, "trenuri");
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    transaction.commit();
                    return true;
                case R.id.navigation_statii:
                    if (statiiFragment == null) {
                        statiiFragment = new StatiiFragment();
                    }
                    transaction.replace(R.id.content, statiiFragment, "statii");
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    transaction.commit();
                    return true;
            }
            return false;
        }
    };
    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
