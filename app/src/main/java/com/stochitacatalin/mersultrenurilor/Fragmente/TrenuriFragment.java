package com.stochitacatalin.mersultrenurilor.Fragmente;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;

import com.stochitacatalin.mersultrenurilor.Adapters.AutoCompleteAdapter;
import com.stochitacatalin.mersultrenurilor.Adapters.StatieAdapter;
import com.stochitacatalin.mersultrenurilor.ArrayListCustom;
import com.stochitacatalin.mersultrenurilor.Date.TrenItem;
import com.stochitacatalin.mersultrenurilor.MainActivity;
import com.stochitacatalin.mersultrenurilor.R;

public class TrenuriFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    public TrenuriFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_trenuri, container, false);
    }
    ArrayListCustom statii = new ArrayListCustom();
    StatieAdapter statiiAdapter;
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        ListView listview = view.findViewById(R.id.listview);
        statiiAdapter = new StatieAdapter(getActivity(), statii);
        listview.setAdapter(statiiAdapter);

        final AutoCompleteTextView trenText = view.findViewById(R.id.trenText);
        trenText.setAdapter(new AutoCompleteAdapter(getActivity(),android.R.layout.simple_dropdown_item_1line,MainActivity.trenurip.trenurip,1));
        trenText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                cautastatii((TrenItem)((AutoCompleteAdapter)trenText.getAdapter()).getItemO(i));
            }
        });
        super.onViewCreated(view,savedInstanceState);
    }
    /*public void cautastatii(String tmp){
        ArrayListCustom ret = MainActivity.trasee.cautastatii(MainActivity.trenuri.getidbynumar(tmp)+1);
        statii.clear();
        for(int i = 0;i<ret.size();i++)
            statii.add(new StatieData(MainActivity.statii.getnumebynumar((Integer) ret.getA(i).get(0)),inttotime((Integer) ret.getA(i).get(2)),inttotime((Integer) ret.getA(i).get(1))));
        statiiAdapter.notifyDataSetChanged();
    }*/
    public void cautastatii(TrenItem tmp){
        ArrayListCustom ret = tmp.cautastatii();
        statii.clear();
        for(int i = 0;i<ret.size();i++)
            statii.add(ret.get(i));
        statiiAdapter.notifyDataSetChanged();
    }
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
