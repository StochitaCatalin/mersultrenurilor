package com.stochitacatalin.mersultrenurilor.Fragmente;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.stochitacatalin.mersultrenurilor.Adapters.TrenAdapter;
import com.stochitacatalin.mersultrenurilor.ArrayListCustom;
import com.stochitacatalin.mersultrenurilor.Date.StatieItem;
import com.stochitacatalin.mersultrenurilor.Date.StatieItemp;
import com.stochitacatalin.mersultrenurilor.Date.TrenData;
import com.stochitacatalin.mersultrenurilor.Date.TrenItem;
import com.stochitacatalin.mersultrenurilor.Date.TrenItemp;
import com.stochitacatalin.mersultrenurilor.MainActivity;
import com.stochitacatalin.mersultrenurilor.R;
import com.stochitacatalin.mersultrenurilor.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import static com.stochitacatalin.mersultrenurilor.Utils.loadfromfile;
import static com.stochitacatalin.mersultrenurilor.Utils.savetofile;

public class AcasaFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public AcasaFragment() {}

    public static AcasaFragment newInstance(String param1, String param2) {
        AcasaFragment fragment = new AcasaFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_acasa, container, false);
    }
    TrenAdapter trenadapter;
    ArrayListCustom<TrenData> trenuri = new ArrayListCustom<>();
    static int trenItemInfo;
    static int plecareItemInfo;
    static int sosireItemInfo;
    static Date dataItemInfo;
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        ListView listview = view.findViewById(R.id.listview);
        trenadapter = new TrenAdapter(getContext(), trenuri,1);
        listview.setAdapter(trenadapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                TrenData tmp = trenadapter.getItem(i);
                TrenInfoFragment fragment = null;
                if (tmp != null) {
                    trenItemInfo = tmp.getTrenItem();
                    plecareItemInfo = tmp.getPlecare();
                    sosireItemInfo = tmp.getSosire();
                    dataItemInfo = tmp.getDate();
                    fragment = TrenInfoFragment.newInstance(2);
                }
                ft.replace(R.id.content,fragment);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        loadfromfile();
        super.onViewCreated(view,savedInstanceState);
    }
    void loadfromfile(){
        trenuri.clear();
        JSONArray jsonArray = Utils.loadfromfile(MainActivity.context,"saved.json");
        int c=0;
        for(int i = 0;i<jsonArray.length();i++){
            Log.d("wh","w");
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int plecare = jsonObject.getInt("plecare");
                int sosire = jsonObject.getInt("sosire");
                TrenItem tren = MainActivity.trenurip.getItem(jsonObject.getInt("tren"));
                Date data = new Date(jsonObject.getLong("data"));
                TrenData temp = new TrenData(jsonObject.getInt("tren"),
                        plecare,
                        tren.statietimp(plecare,1),
                        sosire,tren.statietimp(sosire,2),
                        data);
                long sosireint = data.getTime()/1000 + tren.statietimp(sosire,2);
                if((new Date().getTime()/1000) > sosireint){

                    //delete
                    Log.d(String.valueOf(jsonObject.getInt("tren")),String.valueOf(data.getTime()));
                    jsonArray.remove(i);
                    i--;
                    c++;
                }
                else
                    trenuri.add(temp);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        trenadapter.notifyDataSetChanged();
        if(c>0)
            savetofile(getContext(),"saved.json",jsonArray);
    }
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
       // loadfromfile();
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
