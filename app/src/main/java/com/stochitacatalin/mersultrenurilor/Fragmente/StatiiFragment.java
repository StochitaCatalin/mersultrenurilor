package com.stochitacatalin.mersultrenurilor.Fragmente;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.TabHost;

import com.stochitacatalin.mersultrenurilor.Adapters.AutoCompleteAdapter;
import com.stochitacatalin.mersultrenurilor.Adapters.TrenAdapter;
import com.stochitacatalin.mersultrenurilor.ArrayListCustom;
import com.stochitacatalin.mersultrenurilor.Date.StatieItem;
import com.stochitacatalin.mersultrenurilor.Date.TraseeItem;
import com.stochitacatalin.mersultrenurilor.Date.TrenData;
import com.stochitacatalin.mersultrenurilor.Date.TrenItem;
import com.stochitacatalin.mersultrenurilor.MainActivity;
import com.stochitacatalin.mersultrenurilor.R;

import java.util.Date;

public class StatiiFragment extends Fragment {

    private OnFragmentInteractionListener mListener;

    public StatiiFragment() {}


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_statii, container, false);
    }
    TrenAdapter trenadaptersosiri;
    ArrayListCustom<TrenData> trenurisosiri = new ArrayListCustom<>();
    TrenAdapter trenadapterplecari;
    ArrayListCustom<TrenData> trenuriplecari = new ArrayListCustom<>();

    static int trenItemInfo;
    static int statie;
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        ///Tab Host
        TabHost host = view.findViewById(R.id.tabHost);
        host.setup();
        //Tab 1

        TabHost.TabSpec spec = host.newTabSpec("Tab Sosiri");
        spec.setContent(R.id.Sosiri);
        spec.setIndicator("Sosiri");
        host.addTab(spec);

        //Tab 2
        TabHost.TabSpec spec2 = host.newTabSpec("Tab Plecari");
        spec2.setContent(R.id.Plecari);
        spec2.setIndicator("Plecari");
        host.addTab(spec2);

        //
        ListView listviews = view.findViewById(R.id.listviews);
        ListView listviewp = view.findViewById(R.id.listviewp);
        trenadaptersosiri = new TrenAdapter(getActivity(), trenurisosiri,0);
        listviews.setAdapter(trenadaptersosiri);
        trenadapterplecari = new TrenAdapter(getActivity(), trenuriplecari,0);
        listviewp.setAdapter(trenadapterplecari);
        final AutoCompleteTextView statieText = view.findViewById(R.id.statieText);
        statieText.setAdapter(new AutoCompleteAdapter(getActivity(),android.R.layout.simple_dropdown_item_1line,MainActivity.statiip.statiip,2));
        statieText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //String tmp = (String) statieText.getAdapter().getItem(i);
                cautastatii(((AutoCompleteAdapter) statieText.getAdapter()).getItemI(i));
            }
        });
        listviews.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                TrenData tmp = trenadaptersosiri.getItem(i);
                TrenInfoFragment fragment = null;
                if (tmp != null) {
                    trenItemInfo = tmp.getTrenItem();
                    statie = tmp.getSosire();
                    fragment = TrenInfoFragment.newInstance(3);
                }
                ft.replace(R.id.content,fragment);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        listviewp.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                TrenData tmp = trenadapterplecari.getItem(i);
                TrenInfoFragment fragment = null;
                if (tmp != null) {
                    trenItemInfo = tmp.getTrenItem();
                    statie = tmp.getPlecare();
                    fragment = TrenInfoFragment.newInstance(4);
                }
                ft.replace(R.id.content,fragment);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        super.onViewCreated(view,savedInstanceState);
    }
    public void cautastatii(int statieItem) {
        ArrayListCustom<Integer> trens = MainActivity.statiip.getItem(statieItem).getTrenuri();
        trenurisosiri.clear();
        trenuriplecari.clear();
        for (int i = 0; i < trens.size(); i++) {
            TrenItem trenItem = MainActivity.trenurip.getItem(trens.get(i));
            ArrayListCustom<TraseeItem> tras = trenItem.getTrasee();
            TraseeItem first = tras.get(0);
            TraseeItem ultim = tras.get(tras.size()-1);
            if(!trenItem.getNumar().contains("*") && !trenItem.getNumar().contains("-")) {
                for(int j = 0;j<tras.size();j++){
                    if(tras.get(j).getStatie() == statieItem){
                        if (first.getStatie() != statieItem){
                            TrenData td = new TrenData(trens.get(i), first.getStatie(), first.getOrap(), tras.get(j).getStatie(), tras.get(j).getOras(),new Date());
                            trenurisosiri.add(td);}
                        if (ultim.getStatie() != statieItem){
                            TrenData td = new TrenData(trens.get(i), tras.get(j).getStatie(), tras.get(j).getOrap(), ultim.getStatie(), ultim.getOras(),new Date());
                            trenuriplecari.add(td);}
                    }
                }
            }
        }
        trenadaptersosiri.notifyDataSetChanged();
        trenadapterplecari.notifyDataSetChanged();
    }
    /*public void cautastatii(StatieItem statiiItem) {
        ArrayListCustom stat = MainActivity.trasee.trenuribystatii(statiiItem);
        ArrayListCustom trens = MainActivity.trasee.toatetrenurile(stat);
        trenurisosiri.clear();
        trenuriplecari.clear();
        for (int i = 0; i < trens.size(); i++)
            if (i % 2 == 0)
                trenurisosiri.add(trens.get(i));
            else
                trenuriplecari.add(trens.get(i));
        trenadaptersosiri.notifyDataSetChanged();
        trenadapterplecari.notifyDataSetChanged();
    }*/

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
