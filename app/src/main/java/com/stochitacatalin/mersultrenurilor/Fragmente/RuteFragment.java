package com.stochitacatalin.mersultrenurilor.Fragmente;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ListView;

import com.stochitacatalin.mersultrenurilor.Adapters.AutoCompleteAdapter;
import com.stochitacatalin.mersultrenurilor.Adapters.TrenAdapter;
import com.stochitacatalin.mersultrenurilor.ArrayListCustom;
import com.stochitacatalin.mersultrenurilor.Date.RestrictieItem;
import com.stochitacatalin.mersultrenurilor.Date.StatieItem;
import com.stochitacatalin.mersultrenurilor.Date.StatieItemp;
import com.stochitacatalin.mersultrenurilor.Date.TraseeItem;
import com.stochitacatalin.mersultrenurilor.Date.TrenData;
import com.stochitacatalin.mersultrenurilor.Date.TrenItem;
import com.stochitacatalin.mersultrenurilor.MainActivity;
import com.stochitacatalin.mersultrenurilor.R;

import java.text.ParseException;
import java.util.Date;
import java.util.Objects;

import static com.stochitacatalin.mersultrenurilor.Utils.circulatrenul;
import static com.stochitacatalin.mersultrenurilor.Utils.curentdate;

public class RuteFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    TrenAdapter trenadapter;
    ArrayListCustom<TrenData> trenuri = new ArrayListCustom<>();
    AutoCompleteAdapter fromadapter,toadapter;
    static int trenItemInfo;
    static int plecareItemInfo;
    static int sosireItemInfo;
    static Date dataItemInfo = curentdate();
    CalendarView calendarView;
    public RuteFragment() {}
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_rute, container, false);
    }
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState){

        Button calendar = view.findViewById(R.id.buttoncalendar);
        final CardView calview = view.findViewById(R.id.cvcalendar);
        calendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(calview.getVisibility() == View.GONE)
                    calview.setVisibility(View.VISIBLE);
                else
                    calview.setVisibility(View.GONE);
            }
        });

        calendarView = view.findViewById(R.id.calendarView);

        if(calendarView != null) {
            calendarView.setDate(dataItemInfo.getTime());
            Log.d("ok", String.valueOf(dataItemInfo.getTime()));
        }

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                dataItemInfo = new Date(year - 1900,month,dayOfMonth);
                view.setDate(dataItemInfo.getTime());
                Log.d("changed", String.valueOf(view.getDate()));
                //Search again
                int selfrom = fromadapter.getSelected();
                int selto = toadapter.getSelected();
                if(selfrom != -1 && selto!= -1){
                    cautatrenuri(fromadapter.getItemI(selfrom),toadapter.getItemI(selto));
                }
            }
        });

        ListView listview = view.findViewById(R.id.listview);
        trenadapter = new TrenAdapter(getContext(), trenuri,0);
        listview.setAdapter(trenadapter);
        System.out.println("da");
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                TrenData tmp = trenadapter.getItem(i);
                TrenInfoFragment fragment = null;
                if (tmp != null) {
                    trenItemInfo = tmp.getTrenItem();
                    fragment = TrenInfoFragment.newInstance(1);
                }
                ft.replace(R.id.content,fragment);
                ft.addToBackStack(null);
                ft.commit();
            }
        });
        final AutoCompleteTextView fromtextView = view.findViewById(R.id.fromText);
        final AutoCompleteTextView totextView = view.findViewById(R.id.toText);

        fromtextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                fromadapter.setSelected(i);
                int selfrom = fromadapter.getSelected();
                int selto = toadapter.getSelected();
                if(selfrom != -1 && selto!= -1){
                    cautatrenuri(fromadapter.getItemI(selfrom),toadapter.getItemI(selto));
                }
            }
        });
        totextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                toadapter.setSelected(i);
                int selfrom = fromadapter.getSelected();
                int selto = toadapter.getSelected();

                Log.d(String.valueOf(selfrom), String.valueOf(selto));
                if(selfrom != -1 && selto!= -1){
                    cautatrenuri(fromadapter.getItemI(selfrom),toadapter.getItemI(selto));
                }
            }
        });
        fromadapter =new AutoCompleteAdapter<>(getActivity(),android.R.layout.simple_dropdown_item_1line,MainActivity.statiip.statiip,2);
        toadapter = new AutoCompleteAdapter<>(getActivity(),android.R.layout.simple_dropdown_item_1line, MainActivity.statiip.statiip,2);
        fromtextView.setAdapter(fromadapter);
        totextView.setAdapter(toadapter);
        super.onViewCreated(view,savedInstanceState);
    }
    public void cautatrenuri(int plecare, int sosire){
        plecareItemInfo = plecare;
        sosireItemInfo = sosire;
        trenuri.clear();
        ArrayListCustom<Integer> trenplec = MainActivity.statiip.getItem(plecare).getTrenuri();
        for(int k = 0;k<trenplec.size();k++) {
            boolean status = false;
            int tplecare = 0;
            TrenItem trenp = MainActivity.trenurip.getItem(trenplec.get(k));
            Log.d("nr",trenp.getNumar());
            if(!trenp.getNumar().contains("-") && !trenp.getNumar().contains("*")) {
                ArrayListCustom<TraseeItem> trasee = trenp.getTrasee();
                for (int i = 0; i < trasee.size(); i++) {
                    TraseeItem temp = trasee.get(i);
                    if (temp.getStatie() == plecare) {
                        status = true;
                        tplecare = temp.getOrap();
                    }
                    if (status && temp.getStatie() == sosire) {
                        ArrayListCustom<RestrictieItem> restr = trenp.getRestrictii();
                        boolean ok = false;
                        for (int j = 0; j < restr.size(); j++) {
                            RestrictieItem tmp = restr.get(j);
                            if (circulatrenul(tmp.getDela(), tmp.getPanala(), tmp.getZile(), dataItemInfo)) {
                                ok = true;
                                break;
                            }
                        }
                        if ((ok ||restr.size() == 0))
                            trenuri.add(new TrenData(trenplec.get(k), plecare, tplecare, sosire, temp.getOras(), new Date()));
                    }
                }
            }
        }
        trenadapter.notifyDataSetChanged();
    }
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
