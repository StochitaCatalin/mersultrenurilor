# Mersul Trenurilor

[Descarcă de pe Google Play](https://play.google.com/store/apps/details?id=com.stochitacatalin.mersultrenurilor) - sau caută `Mersul Trenurilor`

##### Date luate de pe [data.gov.ro](http://data.gov.ro/dataset?q=feroviar&sort=score+desc%2C+metadata_modified+desc)

## Fișiere necesare

Această aplicație folosește 6 fișiere pentru a funcționa.

##### Avem 4 fisiere JSON:

### [Trenuri](https://bitbucket.org/StochitaCatalin/mersultrenurilor/src/master/app/src/main/assets/trenuri.json)

Fiecare tren arată în felul următor:

````json
{
	"CodStatieInitiala": "1014",
	"restrictii":[923,65],
	"numar": "15362",
	"numar2": "",
	"categorie": "R",
	"trasee":[13799,1483],
	"CodStatieFinala": "249",
	"operator": "228389"
}
````

- CodStatieInitiala : Stația de unde pleacă trenul(id'ul ei)
- CodStatieFinala : Stația unde trebuie să ajungă trenul(id'ul ei)
- Numar : numărul trenului
- Numar2 : Al doilea număr al trenului(dacă are)
- Categorie : R/IR
- Operator : Id'ul operatorului.În cazul de față Transferoviar Calatori
- Restrictii : Poziția și dimensiunea restricțiilor din fișierul [Restrictii.json](https://bitbucket.org/StochitaCatalin/mersultrenurilor/src/master/app/src/main/assets/restrictii.json)
- Trasee : Poziția și dimensiunea traseelor din fișierul [Trasee.json](https://bitbucket.org/StochitaCatalin/mersultrenurilor/src/master/app/src/main/assets/trasee.json)

##### De ce este necesară poziția si dimensiunea?
Traseul si restricția trenului nu se încarcă la încărcarea trenului ci numai când este nevoie de ele pentru a nu îngreuna timpul de încărcare a aplicației.

### [Restrictii](https://bitbucket.org/StochitaCatalin/mersultrenurilor/src/master/app/src/main/assets/restrictii.json)

Fiecare tren are o listă de restrictii care nu conține nici-o restricție sau mai multe:

````json
[{
		"zile": "1000000",
		"DeLa": "2017-12-10",
		"PanaLa": "2018-06-30"
	}, {
		"zile": "1000000",
		"DeLa": "2018-07-02",
		"PanaLa": "2018-07-07"
	}, {
		"zile": "1000000",
		"DeLa": "2018-07-09",
		"PanaLa": "2018-07-14"
	}
]
````

- Zile : Zilele în care circula trenul
- DeLa : Din ce dată începe restricția
- PanaLa : Când se termină restricția

### [Trasee](https://bitbucket.org/StochitaCatalin/mersultrenurilor/src/master/app/src/main/assets/trasee.json)

Fiecare tren are un traseu, adica o lista de stații:

````json
[{
		"OraS": "-1",
		"Statie": "450",
		"OraP": "58200",
		"TipOprire": "P"
	}, {
		"OraS": "60600",
		"Statie": "316",
		"OraP": "60720",
		"TipOprire": "C"
	}, {
		"OraS": "63360",
		"Statie": "1325",
		"OraP": "-1",
		"TipOprire": "C"
	}
]
````

- OraS : Ora în care sosește trenul în stație(-1 dacă este prima stație)
- OraP : Ora la care pleacă trenul din stație(-1 dacă este ultima stație)
- Statie : Id'ul stației
- TipOprire : Supliment vagoane/Pasageri

### [Statii](https://bitbucket.org/StochitaCatalin/mersultrenurilor/src/master/app/src/main/assets/statii.json)

Fiecare stație este reprezentată în felul următor:

````json
{
	"numar": "44678",
	"trenuri": [238, 249, 251, 255, 256, 264, 266, 276, 282, 287, 288, 290, 296, 298]
}
````

- Numar : Numărul stației
- Trenuri : trenurile care opresc în stație(id'ul lor)

##### Avem 2 fișiere în care ținem poziția stațiilor și a trenurilor dar și numele acestora:

### [Statiip](https://bitbucket.org/StochitaCatalin/mersultrenurilor/src/master/app/src/main/assets/statiip)

````
1 99 Abrami h.
101 64 Abrămuţ hc.
166 74 Aciliu h.
241 69 Aciuţa h.
311 83 Acâş Hm.
395 355 Adjud
751 88 Adjudu Vechi h.
840 100 Adânca PM.
941 84 Adămuş h.
1026 74 Afumaţi h.
1101 49 Agadici h.
1151 317 Aghireş
1469 202 Aghireş h.
````

- Prima coloană reprezintă poziția din fisier
- A doua coloană reprezinta dimensiunea stației
- A treia coloană este numele stației
Deoarece nu încărcăm toate stațiile, numele lor trebuie să fie în acest fișier ci nu în [Statii.json](https://bitbucket.org/StochitaCatalin/mersultrenurilor/src/master/app/src/main/assets/statii.json) deoarece e ineficient sa încărcăm toate stațiile pentru a găsii o stație după numele acesteia.

### [Trenurip](https://bitbucket.org/StochitaCatalin/mersultrenurilor/src/master/app/src/main/assets/trenurip)

````
1 194 IR1698
196 197	IR1695
394 192	15591
587 199	15592
787 199	15593
````

- Prima coloană reprezintă poziția din fisier
- A doua coloană reprezinta dimensiunea trenului
- A treia coloană este numărul trenului
Deoarece nu încărcăm toate trenurile, numărul lor trebuie să fie în acest fișier ci nu în [Trenuri.json](https://bitbucket.org/StochitaCatalin/mersultrenurilor/src/master/app/src/main/assets/trenuri.json) deoarece e ineficient sa încărcăm toate trenurile pentru a găsii un tren după numărul acestuia.

## [MainActivity](https://bitbucket.org/StochitaCatalin/mersultrenurilor/src/master/app/src/main/java/com/stochitacatalin/mersultrenurilor/MainActivity.java)

MainActivity controlează toate fragmentele si conține 2 liste care sunt încărcate din [Trenurip](https://bitbucket.org/StochitaCatalin/mersultrenurilor/src/master/app/src/main/assets/trenurip) și [statiip](https://bitbucket.org/StochitaCatalin/mersultrenurilor/src/master/app/src/main/assets/statiip) la deschiderea aplicației.

## Fragmente

#### Aplicația folosește 5 fragmente:

### [AcasaFragment](https://bitbucket.org/StochitaCatalin/mersultrenurilor/src/master/app/src/main/java/com/stochitacatalin/mersultrenurilor/Fragmente/AcasaFragment.java)


Aici apar trenurile salvate cu următoarele informații:
- Numărul trenului
- Data în care circulă
- Stația de unde pleacă si ora
- Stația unde ajunge si ora
Trenurile salvate sunt încărcate într-o listă din fișierul [saved.json].

| Prima poză | A doua poză |
| :----------: | :-----------: |
|![Acasa1](Screenshots/Acasa1.png)|![Acasa2](Screenshots/Acasa2.png)|

### [RuteFragment](https://bitbucket.org/StochitaCatalin/mersultrenurilor/src/master/app/src/main/java/com/stochitacatalin/mersultrenurilor/Fragmente/RuteFragment.java)

Se poate căuta trenurile care pleacă din stația A si ajung in stația B.Data se poate schimba în funcție de necesitate.

| Prima poză | A doua poză | A treia poză |
| :----------: | :-----------: | :---------: |
|![Rute1](Screenshots/Rute1.png)|![Rute2](Screenshots/Rute2.png)|![Rute3](Screenshots/Rute3.png)|

#####Cum se caută toate traseele?

Odată introdus numele stației de plecare și de sosire, functia cautarute(int,int) este apelată.Funcția necesită id'ul stațiilor(nu numele lor) și verifică dacă trenurile care trec prin stația de plecare ajung în stația de sosire și le adaugă intr-o listă pe care o afișează.
Apăsarea unui item din lista afișată ne deschide fragmentul [TrenInfoFragment](https://bitbucket.org/StochitaCatalin/mersultrenurilor/src/master/app/src/main/java/com/stochitacatalin/mersultrenurilor/Fragmente/TrenInfoFragment.java) care ne arată informații legate de tren.

### [StatiiFragment](https://bitbucket.org/StochitaCatalin/mersultrenurilor/src/master/app/src/main/java/com/stochitacatalin/mersultrenurilor/Fragmente/StatiiFragment.java)

Se pot vedea trenurile care sosesc sau pleacă dintr-o anumită stație.

| Prima poză | A doua poză |
| :----------: | :-----------: |
|![Statie1](Screenshots/Statie1.png)|![Statie2](Screenshots/Statie2.png)|

Deoarece fiecare stație are o listă de trenuri care trec prin ea, listele cu trenurile care pleacă și sosesc se construiește in următorul fel:
1.Se pacurg toate trenurile care trec prin stație
2.Pentru fiecare tren se verifică dacă stația de plecare(initiala) este diferită de stația aleasă, dacă este se adaugă in lista cu trenurile de sosire.
3.Pentru fiecare tren se verifică dacă stația de sosire(finala) este diferită de stația aleasă, dacă este se adaugă in lista cu trenurile de plecare.

### [TrenInfoFragment](https://bitbucket.org/StochitaCatalin/mersultrenurilor/src/master/app/src/main/java/com/stochitacatalin/mersultrenurilor/Fragmente/TrenInfoFragment.java)

Se pot vedea informații despre trenul selectat.

| Prima poză | A doua poză | A treia poză |
| :----------: | :-----------: | :---------: |
|![TrenInfo1](Screenshots/TrenInfo1.png)|![TrenInfo2](Screenshots/TrenInfo2.png)|![TrenInfo3](Screenshots/TrenInfo3.png)|

Se încarcă toate stațiile prin care trece trenul intr-o listă și se afișeaza.
Pentru a salva un tren se apasă pe butonul [Alege tren] si se salvează trenul in fișierul [saved.json].
Dacă fragmentul este deschis din [AcasaFragment] atunci o sa apară un buton pentru a șterge trenul.

### [TrenuriFragment](https://bitbucket.org/StochitaCatalin/mersultrenurilor/src/master/app/src/main/java/com/stochitacatalin/mersultrenurilor/Fragmente/TrenuriFragment.java)

Se pot vedea stațiile în care se oprește un anumit tren.

![Trenuri1](Screenshots/Trenuri1.png)

Se încarcă toate stațiile din trenul curent și se adaugă intr-o listă care se afișează